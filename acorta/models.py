from django.db import models

# Create your models here.
class Contenido(models.Model): #clave primaria de contenidos --> identificador
    url: str = models.CharField(max_length=64)  # Recurso # url introducida en el POST
    short = models.CharField(max_length=64, blank= True) # url acortada

class Shortvacio(models.Model): #me creo tabla de urls acortadas vacias
    short = models.CharField(max_length=64, blank=True)

    #def __str__(self):
        #return str(self.id) + ": " + self.recurso