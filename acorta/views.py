from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponsePermanentRedirect
from .models import Contenido, Shortvacio
from .forms import InformacionForm
from django.template import loader
from django.views.decorators.csrf import csrf_exempt  # Saltar mecanismo de seguridad CSRF

# Create your views here.

def comprobacion_formato(url, short):
    if not (url.startswith("http://") or url.startswith("https://")):
        url = "http://" + url
        short = 'http://localhost:8000/' + str(short)
    else:
        short = "http://localhost:8000/'" + str(short)
    return url, short

def manage_shortvacio(short):
    shortvacio = Shortvacio(short=short) #me creo nueva fila en la tabla shortvacio
    shortvacio.save() #guardo la informacion
    short = shortvacio.id  #añado valor a short y lo devuelvo
    return short

def comprobacion_urlnueva(url, urls, short):
    if url in urls: #si la url ya ha sido guardada antes, actualizo el valor de short
        contenido = Contenido.objects.get(url=url)
        contenido.short = short
        contenido.save()
    else:
        urlactualizada = Contenido(short=short, url=url) #si es nueva, la añado a la tabla
        urlactualizada.save()

@csrf_exempt
def index(request): #recibo localhost:8000/acorta/
    formulario_vacio = InformacionForm() #muestro un formulario vacío
    if request.method == 'POST':
        respuestas = InformacionForm(request.POST) #Extraigo las respuestas del formulario
        if respuestas.is_valid(): #Si las respuestas son válidas
            short = respuestas.cleaned_data.get('short')
            url = respuestas.cleaned_data.get('url')
            if short == '': #Si el campo short estaba vacio, asigno nuevo valor a short
                short = manage_shortvacio(short)  # lo guardo en la tabla correspondiente
            url, short = comprobacion_formato(url, short) #compruebo formato de url y de short
            urls = Contenido.objects.values_list('url', flat=True)  # lista con todas las urls guardadas
            comprobacion_urlnueva(url, urls, short)
            respuestas_guardadas = Contenido.objects.all() #Extraigo la tabla contenido
            template = loader.get_template('acorta/formulario.html') #Cargo el formulario
            contexto = {'respuestas_guardadas': respuestas_guardadas, 'formulario_vacio': formulario_vacio}
            return HttpResponse(template.render(contexto, request))
        else:
            raise Http404('La url introducida no es válida')
    else:  # La primera vez que entro en la página
        respuestas_guardadas = Contenido.objects.all()
        context = {'respuestas_guardadas': respuestas_guardadas, 'formulario_vacio': formulario_vacio}
        return render(request, 'acorta/formulario.html', context)

@csrf_exempt
def get_resource(request, recurso):
    # GET
    try: #localhost:8000/acorta/google.com
        recurso = f'http://localhost:8000/{recurso}'
        contenido = Contenido.objects.get(short=recurso) #si el recurso introducido ya existía -->
        respuesta = HttpResponsePermanentRedirect(contenido.url)                    # reedirección
        return respuesta
    except Contenido.DoesNotExist:
        raise Http404(f'<h1>Recurso no disponible<h1>')
