# Generated by Django 4.1.7 on 2023-03-26 13:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acorta', '0002_contenido_recurso_alter_contenido_clave'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contenido',
            name='recurso',
        ),
        migrations.AlterField(
            model_name='contenido',
            name='clave',
            field=models.CharField(max_length=64),
        ),
    ]
