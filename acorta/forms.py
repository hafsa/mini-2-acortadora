from django import forms
from .models import Contenido


class InformacionForm(forms.ModelForm):
    class Meta:
        model = Contenido
        fields = ['url', 'short']

    widgets = {
        'url': forms.TextInput(attrs={'placeholder': 'Url'}),
        'short': forms.TextInput(attrs={'placeholder': 'Short (opcional)'}),
    }
